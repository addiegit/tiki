$('body').scrollspy({ target: '#navegacion' })
$('[data-spy="scroll"]').each(function () {
    var $spy = $(this).scrollspy('refresh')
  })

// Editar Palabra
function editar(id,titulo,descripcion,estado){
  $("#editId").val(id)
  $("#editTitulo").val(titulo)
  $("#editDescripcion").val(descripcion)
  $("#editEstado").val(estado)
}
// Editar Frase
function editarF(id,titulo,descripcion,estado){
  $("#editIdF").val(id)
  $("#editTituloF").val(titulo)
  $("#editDescripcionF").val(descripcion)
  $("#editEstadoF").val(estado)
}
// Editar Noticia
function editarN(id,titulo,descripcion,estado,tag){
  $("#editIdN").val(id)
  $("#editTituloN").val(titulo)
  $("#editDescripcionN").val(descripcion)
  $("#editEstadoN").val(estado)
  $("#editTagN").val(tag)
  $("#editTagN").val(tag)
}

//Validación formularios
//Diccionario
$(function() {
  $("#nuevaPalabra").validate({
    rules: {
      titulo:{
        required: true,
        minlength: 2,
        maxlength: 40
      },
      descripcion:{
        required: true,
        minlength: 4,
        maxlength: 500
      }
    },
    messages:{
      titulo:{
        required: "Ingrese un título.",
        minlength: "El título debe contener almenos 2 letras.",
        maxlength: "No puedes exceder los 40 caracteres."
      },
      descripcion:{
        required: "Ingrese una descripción.",
        minlength: "Ingresa una descripción más larga.",
        maxlength: "No puedes exceder los 500 caracteres."
      }
    }
  });
});

// Editar Palabra
$(function() {
  $("#editarPalabra").validate({
    rules: {
      editTitulo:{
        required: true,
        minlength: 2,
        maxlength: 40
      },
      editDescripcion:{
        required: true,
        minlength: 4,
        maxlength: 500
      },
      editEstado:{
        required: true
      }
    },
    messages:{
      editTitulo:{
        required: "Ingrese un título.",
        minlength: "El título debe contener almenos 2 letras.",
        maxlength: "No puedes exceder los 40 caracteres."
      },
      editDescripcion:{
        required: "Ingrese una descripción",
        minlength: "Ingresa una descripción más larga.",
        maxlength: "No puedes exceder los 500 caracteres."
      },
      editEstado:{
        required: "Seleccione un estado."
      }
    }
  });
});

// Frases
$(function() {
  $("#nuevaFrase").validate({
    rules: {
      titulo:{
        required: true,
        minlength: 2,
        maxlength: 40
      },
      descripcion:{
        required: true,
        minlength: 4,
        maxlength: 500
      }
    },
    messages:{
      titulo:{
        required: "Ingrese un título.",
        minlength: "El título debe contener almenos 2 letras.",
        maxlength: "No puedes exceder los 40 caracteres."
      },
      descripcion:{
        required: "Ingrese una descripción",
        minlength: "Ingresa una descripción más larga.",
        maxlength: "No puedes exceder los 500 caracteres."
      }
    }
  });
});

// Editar Frase
$(function() {
  $("#editarFrase").validate({
    rules: {
      editTituloF:{
        required: true,
        minlength: 2,
        maxlength: 40
      },
      editDescripcionF:{
        required: true,
        minlength: 4,
        maxlength: 500
      },
      editEstadoF:{
        required: true
      }
    },
    messages:{
      editTituloF:{
        required: "Ingrese un título",
        minlength: "El título debe contener almenos 2 letras.",
        maxlength: "No puedes exceder los 40 caracteres."
      },
      editDescripcionF:{
        required: "Ingrese una descripción",
        minlength: "Ingresa una descripción más larga.",
        maxlength: "No puedes exceder los 500 caracteres."
      },
      editEstadoF:{
        required: "Seleccione un estado."
      }
    }
  });
});

// Agregar Noticia
$(function() {
  $( "#nuevaNoticia" ).validate({
    rules: {
      titulo:{
        required: true,
        minlength: 2,
        maxlength: 40
      },
      descripcion:{
        required: true,
        minlength: 4,
        maxlength: 500
      },
      foto:{
        required: true,
        extension: "jpg|png"
      }
    },
    messages:{
      titulo:{
        required: "Ingrese un título",
        minlength: "El título debe contener almenos 2 letras.",
        maxlength: "No puedes exceder los 40 caracteres."
      },
      descripcion:{
        required: "Ingrese una descripción",
        minlength: "Ingresa una descripción más larga.",
        maxlength: "No puedes exceder los 500 caracteres."
      },
      foto:{
        required: "Suba una foto para la noticia.",
        extension: "La foto debe ser JPG o PNG."
      }
    }
  });
});

// Editar Noticia
$(function() {
  $("#editarNoticia").validate({
    rules: {
      editTituloN:{
        required: true,
        minlength: 2,
        maxlength: 40
      },
      editDescripcionN:{
        required: true,
        minlength: 4,
        maxlength: 500
      },
      editEstadoN:{
        required: true
      },
      editTagN:{
        requiered: true
      },
      editFoto:{
        required: true
      }
    },
    messages:{
      editTituloN:{
        required: "Ingrese un título.",
        minlength: "El título debe contener almenos 2 letras.",
        maxlength: "No puedes exceder los 40 caracteres."
      },
      editDescripcionN:{
        required: "Ingrese una descripción",
        minlength: "Ingresa una descripción más larga.",
        maxlength: "No puedes exceder los 500 caracteres."
      },
      editEstadoN:{
        required: "Seleccione un estado."
      },
      editTagN:{
        required: "Seleccione un TAG."
      },
      editFoto:{
        required: "Suba una foto para la noticia.",
        extension: "La foto debe ser JPG o PNG."
      }
    }
  });
});

// Login
$(function() {
  $("#loginForm").validate({
    rules: {
      email:{
        required: true,
        email: true
      },
      contrasenia:{
        required: true
      },
    },
    messages:{
      email:{
        required: "Ingrese su email.",
        email: "Ingrese una dirección de correo válida."
      },
      contrasenia:{
        required: "Ingrese su contraseña."
      }
    }
  });
});


// Registro
$(function() {
  $("#formulario").validate({
    rules: {
      email:{
        required: true,
        email: true
      },
      contrasena:{
        required: true,
      },
      cpassword:{
        required: true,
        equalTo : "#contrasena"
      }
    },
    messages:{
      email:{
        required: "Ingrese su email.",
        email: "Ingrese una dirección de correo válida."
      },
      contrasena:{
      required: "Ingrese su contraseña."
      },
      cpassword:{
        required: "Confirme su contraseña.",
        equalTo: "Las contraseñas deben concidir."
      }
    }
  });
});