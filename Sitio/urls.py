from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('Diccionario', views.diccionario, name="diccionario"),
    path('Frases', views.frases, name="frases"),
    path('Pendientes', views.pendientes, name="pendientes"),
    path('Registro', views.registro, name="registro"),

    path('Noticia/Crear', views.nuevaNoticia, name="nuevaNoticia"),
    path('Noticia/Publicar/<int:id>', views.publicarNoticia, name="publicarNoticia"),
    path('Noticia/Editar/', views.editarNoticia, name="editarNoticia"),
    path('Noticia/Borrar/<int:id>', views.borrarNoticia, name="borrarNoticia"),

    path('Diccionario/Crear', views.nuevaPalabra, name="nuevaPalabra"),
    path('Diccionario/Publicar/<int:id>', views.publicarPalabra, name="publicarPalabra"),
    path('Diccionario/Editar/', views.editarPalabra, name="editarPalabra"),
    path('Diccionario/Borrar/<int:id>', views.borrarPalabra, name="borrarPalabra"),

    path('Frases/Crear', views.nuevaFrase, name="nuevaFrase"),
    path('Frases/Publicar/<int:id>', views.publicarFrase, name="publicarFrase"),
    path('Frases/Editar/', views.editarFrase, name="editarFrase"),
    path('Frases/Borrar/<int:id>', views.borrarFrase, name="borrarFrase"),

    path('Registro/Crear', views.crear, name="crear"),
    path('Login', views.login, name="login"),
    path('Login/Iniciar',views.loginIniciar,name="loginIniciar"),
    path('Logout',views.cerrarSession,name="cerrarSession"),
]